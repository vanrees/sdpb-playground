using LinearAlgebra
using Unmarshal
import JSON
# print 10 digits for bigfloats
Base.print(io::IO, b::BigFloat) = print(io, Base.MPFR._string(b,10))

# todos:
# - corrector step
# - verification against SDPB
# - multiple correlators (bigger than 1x1 PVMs)
# - command line args
# - perhaps some parallelization
# - smaller todos in the code

# ------------------------------------------------------------------------------
# parameters, including precision
# ------------------------------------------------------------------------------

# Warning: use of unexported @kwdef. Might stop working.
Base.@kwdef struct SDPparams
    initialscale_primal::BigFloat = 1e10
    initialscale_dual::BigFloat = 1e10
    primal_error_threshold::BigFloat = 1e-30
    dual_error_threshold::BigFloat = 1e-30
    duality_gap_threshold::BigFloat = 1e-30
    find_primal_feasible::Bool = false
    find_dual_feasible::Bool = false
    infeasible_centering_parameter::BigFloat = 0.3
    step_length_reduction::BigFloat = 0.7
    max_iterations::Int = 200
    precision::Int = 512
end

sdpparams = SDPparams()

setprecision(sdpparams.precision)

# ------------------------------------------------------------------------------
# basic structures
# ------------------------------------------------------------------------------

# all the constant properties of a PVM
struct PVM
    range_i_v::Int
    range_i_w::Int
    range_alpha::Int
    # v_{alpha i}, i = 1..range_i_v
    v::Matrix{BigFloat}
    # w_{alpha i}, i = 1..range_i_w
    w::Matrix{BigFloat}
    # c_{alpha}
    c::Vector{BigFloat}
    # B_{a alpha}
    B::Matrix{BigFloat}
end

# all the constant properties of an SDP
struct SDP
    size::Int
    b0::BigFloat
    # b_a, a = 1..size
    b::Vector{BigFloat}
    pvms::Vector{PVM}
end

# contains dx, dy, dX, dY
mutable struct PVMdeltas
    x::Vector{BigFloat}
    Xv::Matrix{BigFloat}
    Xw::Matrix{BigFloat}
    Yv::Matrix{BigFloat}
    Yw::Matrix{BigFloat}
end

mutable struct PVMresidues
    # t_alpha: c - B y - tr(Yv V) - tr(Yw W)
    t::Vector{BigFloat}
    # Pv_{ij}: - Xv + x V
    Pv::Matrix{BigFloat}
    # Pw_{ij}: - Xw + x W
    Pw::Matrix{BigFloat}
    # R_{ij}: XY (note: without subtracting the mu I) 
    Rv::Matrix{BigFloat}
    Rw::Matrix{BigFloat}
end

mutable struct PVMauxs
    Xv_inv::Matrix{BigFloat}
    Xw_inv::Matrix{BigFloat}
    M_inv::Matrix{BigFloat}
    t_tilde::Vector{BigFloat}
    Pv_tilde::Matrix{BigFloat}
    Pw_tilde::Matrix{BigFloat}
end

# the variable properties of a PVM
mutable struct PVMvars
    x::Vector{BigFloat}
    Xv::Matrix{BigFloat}
    Xw::Matrix{BigFloat}
    Yv::Matrix{BigFloat}
    Yw::Matrix{BigFloat}
    auxs::PVMauxs
    deltas::PVMdeltas
    residues::PVMresidues
end

mutable struct SDPindicators
    primal_objective::BigFloat
    dual_objective::BigFloat
    dual_error::BigFloat
    primal_error_constr::BigFloat
    primal_error_matrix::BigFloat
    primal_step_length::BigFloat
    dual_step_length::BigFloat
    duality_gap::BigFloat
    mu_observed::BigFloat
    terminatereason::String
end

# the variable properties of an SDP
mutable struct SDPvars
    y::Vector{BigFloat}
    pvmvarss::Vector{PVMvars} # not a typo, just awkward
    mu_target::BigFloat
    # deltas: no need for a separate struct as for the PVMs
    delta_y::Vector{BigFloat}
    # residues: no need for a separate struct as for the PVMs
    # d_a: b - B^T x
    residue_d::Vector{BigFloat}
    # for progress tracking
    indicators::SDPindicators
end


# ------------------------------------------------------------------------------
# Reading (json) input files
# ------------------------------------------------------------------------------

struct PVMinput
    # should be 1
    dim::Int
    # range of alpha
    num_points::Int
    # v_{alpha i}
    bilinear_bases_even::Matrix{BigFloat}
    # w_{alpha i}
    bilinear_bases_odd::Matrix{BigFloat}
    # c_{alpha}
    c::Vector{BigFloat}
    # B_{a alpha}
    B::Matrix{BigFloat}
end

struct SDPinput
    # range of a
    size::Int
    # total size of X or Y
    constant::BigFloat
    b::Vector{BigFloat}
    pvms::Vector{PVMinput}
end

function readSDPinput(filepath)
    pvms = PVMinput[]
    i = 0
    while true
        blockfilename = string(filepath,"block_",i,".json")
        if !(isfile(blockfilename))
            break
        end
        blockdict = JSON.parsefile(blockfilename)
        newpvm = Unmarshal.unmarshal(PVMinput,blockdict)
        println("block: ", i)
        println("dim: ", newpvm.dim)
        println("num_points: ", newpvm.num_points)
        println("even_size: ",size(newpvm.bilinear_bases_even))
        println("odd_size: ",size(newpvm.bilinear_bases_odd))
        println("c: ",length(newpvm.c))
        println("B: ",size(newpvm.B),"\n")
        if !(newpvm.dim==1)
            error("Higher-dimensional problems are not yet supported.")
        end
        push!(pvms,newpvm)
        i += 1
    end

    objdict = JSON.parsefile(string(filepath, "objectives.json"))
    println("\nobjective: ", length(objdict["b"]))

    SDPinput(length(objdict["b"]),
             BigFloat(objdict["constant"]),
             BigFloat.(objdict["b"]),
             pvms)
end

function toPVM(pvminput)
    range_alpha = pvminput.num_points

    # todo: maybe no deepcopy?
    v = deepcopy(pvminput.bilinear_bases_even)
    range_i_v = size(v,2)

    w = deepcopy(pvminput.bilinear_bases_odd)
    range_i_w  = size(w,2)

    PVM(range_i_v,
        range_i_w,
        range_alpha,
        v,
        w,
        pvminput.c,
        pvminput.B)
end

function toSDP(sdpinput)
    SDP(sdpinput.size,sdpinput.constant,sdpinput.b,map(toPVM,sdpinput.pvms))
end

# ------------------------------------------------------------------------------
# Initialize variable part of an SDP - some randomization here
# ------------------------------------------------------------------------------

function initializeSDPvars(sdp)
    pvmvarss = []

    # todo: initialize properly

    for pvm in sdp.pvms
        x = ones(BigFloat,pvm.range_alpha)

        Xv = zeros(pvm.range_i_v,pvm.range_i_v)
        Xw = zeros(pvm.range_i_w,pvm.range_i_w)
        Xv += sdpparams.initialscale_primal * I
        Xw += sdpparams.initialscale_primal * I

        Yv = zeros(pvm.range_i_v,pvm.range_i_v)
        Yw = zeros(pvm.range_i_w,pvm.range_i_w)
        Yv += sdpparams.initialscale_dual * I
        Yw += sdpparams.initialscale_dual * I

        Xv_inv = Array{BigFloat,2}(undef,pvm.range_i_v,pvm.range_i_v)
        Xw_inv = Array{BigFloat,2}(undef,pvm.range_i_w,pvm.range_i_w)
        M_inv = Array{BigFloat,2}(undef,pvm.range_alpha,pvm.range_alpha)
        t_tilde = Array{BigFloat,1}(undef,pvm.range_alpha)
        Pv_tilde = Array{BigFloat,2}(undef,pvm.range_i_v,pvm.range_i_v)
        Pw_tilde = Array{BigFloat,2}(undef,pvm.range_i_w,pvm.range_i_w)
                
        delta_x = Array{BigFloat,1}(undef,pvm.range_alpha)
        delta_Xv = Array{BigFloat,2}(undef,pvm.range_i_v,pvm.range_i_v)
        delta_Xw = Array{BigFloat,2}(undef,pvm.range_i_w,pvm.range_i_w)
        delta_Yv = Array{BigFloat,2}(undef,pvm.range_i_v,pvm.range_i_v)
        delta_Yw = Array{BigFloat,2}(undef,pvm.range_i_w,pvm.range_i_w)
        
        t = Array{BigFloat,1}(undef,pvm.range_alpha)
        Pv = Array{BigFloat,2}(undef,pvm.range_i_v,pvm.range_i_v)
        Pw = Array{BigFloat,2}(undef,pvm.range_i_v,pvm.range_i_v)
        Rv = Array{BigFloat,2}(undef,pvm.range_i_v,pvm.range_i_v)
        Rw = Array{BigFloat,2}(undef,pvm.range_i_w,pvm.range_i_w)

        pvmvars = PVMvars(x,
                          Xv,
                          Xw,
                          Yv,
                          Yw,
                          PVMauxs(Xv_inv,
                                  Xw_inv,
                                  M_inv,
                                  t_tilde,
                                  Pv_tilde,
                                  Pw_tilde),
                          PVMdeltas(delta_x,
                                    delta_Xv,
                                    delta_Xw,
                                    delta_Yv,
                                    delta_Yw),
                          PVMresidues(t,
                                      Pv,
                                      Pw,
                                      Rv,
                                      Rw)
                          )

        push!(pvmvarss,pvmvars)
    end

    y = ones(BigFloat,sdp.size)
    delta_y = Array{BigFloat}(undef,sdp.size)
    residue_d = Array{BigFloat}(undef,sdp.size)
    # 0 should be undefined but I do not yet know how this works
    sdpvars = SDPvars(y,pvmvarss,0,delta_y,residue_d,SDPindicators(0,0,0,0,0,0,0,0,0,"not done yet..."))
    return(sdpvars)
end

# ------------------------------------------------------------------------------
# Take one step, update variables in place
# ------------------------------------------------------------------------------

function step!(sdp, sdpvars)
    # --------------------------------------------------------------------------
    # compute residues
    # --------------------------------------------------------------------------
    for (pvm, pvmvars) in zip(sdp.pvms, sdpvars.pvmvarss)
        # t
        pvmvars.residues.t = deepcopy(pvm.c)
        pvmvars.residues.t -= pvm.B' * sdpvars.y
        for alpha = 1:pvm.range_alpha
            pvmvars.residues.t[alpha] -= pvm.v[alpha,:]' * pvmvars.Yv * pvm.v[alpha,:]
            pvmvars.residues.t[alpha] -= pvm.w[alpha,:]' * pvmvars.Yw * pvm.w[alpha,:]
        end
        
        # Pv
        pvmvars.residues.Pv = deepcopy(pvmvars.Xv) 
        pvmvars.residues.Pv *= -1
        for alpha = 1:pvm.range_alpha
            # todo: store outer product evaluations ?
            pvmvars.residues.Pv += pvmvars.x[alpha] * pvm.v[alpha,:] .* pvm.v[alpha,:]' 
        end

        # Pw
        pvmvars.residues.Pw = deepcopy(pvmvars.Xw) 
        pvmvars.residues.Pw *= -1
        for alpha = 1:pvm.range_alpha
            # todo: store outer product evaluations ?
            pvmvars.residues.Pw += pvmvars.x[alpha] * pvm.w[alpha,:] .* pvm.w[alpha,:]' 
        end

        # R
        # todo: eliminate
        pvmvars.residues.Rv = pvmvars.Xv * pvmvars.Yv
        pvmvars.residues.Rw = pvmvars.Xw * pvmvars.Yw
    end

    # d
    sdpvars.residue_d = deepcopy(sdp.b)
    for (pvm, pvmvars) in zip(sdp.pvms, sdpvars.pvmvarss)
        sdpvars.residue_d -= pvm.B * pvmvars.x
    end

    # --------------------------------------------------------------------------
    # compute indicators 
    # --------------------------------------------------------------------------

    # primal_objective
    sdpvars.indicators.primal_objective = sdp.b0
    for (pvm, pvmvars) in zip(sdp.pvms, sdpvars.pvmvarss)
        sdpvars.indicators.primal_objective += pvm.c' * pvmvars.x
    end
    
    # dual_objective
    sdpvars.indicators.dual_objective = sdp.b0 + sdp.b' * sdpvars.y

    # duality_gap
    sdpvars.indicators.duality_gap = sdpvars.indicators.primal_objective - sdpvars.indicators.dual_objective

    # primal_error_constr
    sdpvars.indicators.primal_error_constr = maximum(abs.(sdpvars.residue_d))

    # primal_error_matrix, dual_error, mu_observed
    sdpvars.indicators.primal_error_matrix = 0
    sdpvars.indicators.dual_error = 0
    let mu_observed_numerator = 0, mu_observed_denominator = 0
        for (pvm, pvmvars) in zip(sdp.pvms, sdpvars.pvmvarss)
            sdpvars.indicators.primal_error_matrix = max(sdpvars.indicators.primal_error_matrix, maximum(abs.(pvmvars.residues.Pv)))
            sdpvars.indicators.primal_error_matrix = max(sdpvars.indicators.primal_error_matrix, maximum(abs.(pvmvars.residues.Pw)))
            sdpvars.indicators.dual_error = max(sdpvars.indicators.dual_error, maximum(abs.(pvmvars.residues.t)))
            mu_observed_numerator += tr(pvmvars.residues.Rv) + tr(pvmvars.residues.Rw)
            mu_observed_denominator += pvm.range_i_v + pvm.range_i_w
        end
        sdpvars.indicators.mu_observed = mu_observed_numerator / BigFloat(mu_observed_denominator)
    end

    # --------------------------------------------------------------------------
    # are we there yet?
    # --------------------------------------------------------------------------
    primal_feasible = false
    if max(sdpvars.indicators.primal_error_constr, sdpvars.indicators.primal_error_matrix) < sdpparams.primal_error_threshold
        if sdpparams.find_primal_feasible
            sdpvars.terminatereason = "Found primal feasible point"
            return true
        end
        primal_feasible = true
    end

    dual_feasible = false
    if sdpvars.indicators.dual_error < sdpparams.dual_error_threshold
        if sdpparams.find_dual_feasible
            sdpvars.terminatereason = "Found dual feasible point"
            return true
        end
        dual_feasible = true
    end

    if primal_feasible && dual_feasible && sdpvars.indicators.duality_gap < sdpparams.duality_gap_threshold
        sdpvars.terminatereason = "Found primal-dual optimal solution"
        return true
    end

    # --------------------------------------------------------------------------
    # set target mu
    # --------------------------------------------------------------------------
    mu = sdpvars.indicators.mu_observed * sdpparams.infeasible_centering_parameter
    if primal_feasible && dual_feasible
        mu = 0
    end

    # --------------------------------------------------------------------------
    # computation of auxiliary variables
    # --------------------------------------------------------------------------
    for (pvm, pvmvars) in zip(sdp.pvms, sdpvars.pvmvarss)
        pvmvars.auxs.Xv_inv = inv(pvmvars.Xv)

        pvmvars.auxs.Xw_inv = inv(pvmvars.Xw)

        pvmvars.auxs.Pv_tilde = mu * pvmvars.auxs.Xv_inv
        pvmvars.auxs.Pv_tilde -= pvmvars.Yv
        pvmvars.auxs.Pv_tilde -= pvmvars.auxs.Xv_inv * pvmvars.residues.Pv * pvmvars.Yv

        pvmvars.auxs.Pw_tilde = mu * pvmvars.auxs.Xw_inv
        pvmvars.auxs.Pw_tilde -= pvmvars.Yw
        pvmvars.auxs.Pw_tilde -= pvmvars.auxs.Xw_inv * pvmvars.residues.Pw * pvmvars.Yw

        pvmvars.auxs.t_tilde = deepcopy(pvmvars.residues.t)
        for alpha = 1:pvm.range_alpha
            pvmvars.auxs.t_tilde[alpha] -= pvm.v[alpha,:]' * pvmvars.auxs.Pv_tilde * pvm.v[alpha,:]
            pvmvars.auxs.t_tilde[alpha] -= pvm.w[alpha,:]' * pvmvars.auxs.Pw_tilde * pvm.w[alpha,:]
        end
        
        # M_inv
        Xv_inv_hat = pvm.v * pvmvars.auxs.Xv_inv * pvm.v'
        Xw_inv_hat = pvm.w * pvmvars.auxs.Xw_inv * pvm.w'
        Yv_hat = pvm.v * pvmvars.Yv * pvm.v'
        Yw_hat = pvm.w * pvmvars.Yw * pvm.w'
        M = zeros(BigFloat,pvm.range_alpha,pvm.range_alpha)
        M += Xv_inv_hat .* Yv_hat
        M += Xw_inv_hat .* Yw_hat
        # todo: more efficient solution
        pvmvars.auxs.M_inv = inv(M)
    end
    
    # --------------------------------------------------------------------------
    # computation of delta_x, delta_X, delta_y, delta_Y
    # --------------------------------------------------------------------------
    # delta_y
    let u, N
        N = zeros(BigFloat,sdp.size,sdp.size)
        u = deepcopy(sdpvars.residue_d)
        for (pvm, pvmvars) in zip(sdp.pvms, sdpvars.pvmvarss)
            u += pvm.B * pvmvars.auxs.M_inv * pvmvars.auxs.t_tilde
            N += pvm.B * pvmvars.auxs.M_inv * pvm.B'
        end
        sdpvars.delta_y = N \ u
    end

    # delta_x, delta_Xv, delta_Xw, delta_Yv, delta_Yw
    for (pvm, pvmvars) in zip(sdp.pvms, sdpvars.pvmvarss)
        # todo: double check
        pvmvars.deltas.x = pvmvars.auxs.M_inv * (pvm.B' * sdpvars.delta_y - pvmvars.auxs.t_tilde)

        pvmvars.deltas.Xv = deepcopy(pvmvars.residues.Pv)
        for alpha = 1:pvm.range_alpha
            # todo: maybe store outer products
            pvmvars.deltas.Xv += pvmvars.deltas.x[alpha] * pvm.v[alpha,:] .* pvm.v[alpha,:]' 
        end

        pvmvars.deltas.Xw = deepcopy(pvmvars.residues.Pw)
        for alpha = 1:pvm.range_alpha
            pvmvars.deltas.Xw += pvmvars.deltas.x[alpha] * pvm.w[alpha,:] .* pvm.w[alpha,:]' 
        end

        pvmvars.deltas.Yv = mu * pvmvars.auxs.Xv_inv - pvmvars.Yv - pvmvars.auxs.Xv_inv * pvmvars.deltas.Xv * pvmvars.Yv
        pvmvars.deltas.Yv += transpose(pvmvars.deltas.Yv)
        pvmvars.deltas.Yv /= 2

        pvmvars.deltas.Yw = mu * pvmvars.auxs.Xw_inv - pvmvars.Yw - pvmvars.auxs.Xw_inv * pvmvars.deltas.Xw * pvmvars.Yw
        pvmvars.deltas.Yw += transpose(pvmvars.deltas.Yw)
        pvmvars.deltas.Yw /= 2
    end

    # --------------------------------------------------------------------------
    # computation of step length
    # --------------------------------------------------------------------------

    let min_ev_primal = 0, min_ev_dual = 0
        # computing smallest eigenvalues
        for pvmvars in sdpvars.pvmvarss
            # todo: get rid of conversion to float; need arbitrary precision eigvals.

            # cholesky reminder: Xv.L * Xv.U = Xv
            Xv_chol = cholesky(Symmetric(pvmvars.Xv))
            min_ev_Xv = minimum(eigvals(convert(Array{Float64},Symmetric(inv(Xv_chol.L) * pvmvars.deltas.Xv * inv(Xv_chol.U)))))
            Xw_chol = cholesky(Symmetric(pvmvars.Xw))
            min_ev_Xw = minimum(eigvals(convert(Array{Float64},Symmetric(inv(Xw_chol.L) * pvmvars.deltas.Xw * inv(Xw_chol.U)))))
            min_ev_X = min(min_ev_Xv, min_ev_Xw)

            Yv_chol = cholesky(Symmetric(pvmvars.Yv))
            min_ev_Yv = minimum(eigvals(convert(Array{Float64},Symmetric(inv(Yv_chol.L) * pvmvars.deltas.Yv * inv(Yv_chol.U)))))
            Yw_chol = cholesky(Symmetric(pvmvars.Yw))
            min_ev_Yw = minimum(eigvals(convert(Array{Float64},Symmetric(inv(Yw_chol.L) * pvmvars.deltas.Yw * inv(Yw_chol.U)))))
            min_ev_Y = min(min_ev_Yv, min_ev_Yw)

            min_ev_primal = min(min_ev_primal,min_ev_X)
            min_ev_dual = min(min_ev_dual,min_ev_Y)
        end

        # computing step lengths, with rescalings
        if min_ev_primal >= 0
            sdpvars.indicators.primal_step_length = 1
        else
            sdpvars.indicators.primal_step_length = min(1, -1/min_ev_primal)
        end
        sdpvars.indicators.primal_step_length *= sdpparams.step_length_reduction

        if min_ev_dual >= 0
            sdpvars.indicators.dual_step_length = 1
        else
            sdpvars.indicators.dual_step_length = min(1, -1/min_ev_dual)
        end
        sdpvars.indicators.dual_step_length *= sdpparams.step_length_reduction
    end # of let block

    # --------------------------------------------------------------------------
    # step
    # --------------------------------------------------------------------------
    sdpvars.y += sdpvars.indicators.dual_step_length * sdpvars.delta_y
    for pvmvars in sdpvars.pvmvarss
        pvmvars.x += sdpvars.indicators.primal_step_length * pvmvars.deltas.x
        pvmvars.Xv += sdpvars.indicators.primal_step_length * pvmvars.deltas.Xv
        pvmvars.Xw += sdpvars.indicators.primal_step_length * pvmvars.deltas.Xw
        pvmvars.Yv += sdpvars.indicators.dual_step_length * pvmvars.deltas.Yv
        pvmvars.Yw += sdpvars.indicators.dual_step_length * pvmvars.deltas.Yw
    end

    println("mu:", sdpvars.indicators.mu_observed)
    println("duality gap:", sdpvars.indicators.duality_gap)
    println("primal error:", max(sdpvars.indicators.primal_error_constr, sdpvars.indicators.primal_error_matrix))
    println("dual error:", sdpvars.indicators.dual_error)
    println("primal objective:", sdpvars.indicators.primal_objective)
    println("dual objective:", sdpvars.indicators.dual_objective)
    println("primal step length:", sdpvars.indicators.primal_step_length)
    println("dual step length:", sdpvars.indicators.dual_step_length)

    return false
end

sdp = toSDP(readSDPinput("/Users/vanrees/Dropbox/julia/tests/2d_maxOPE_json/"))
sdpvars = initializeSDPvars(sdp)

i = 1
while true
    println("i = ", i)
    if step!(sdp,sdpvars) || i > sdpparams.max_iterations
        break
    end
    global i += 1
end
